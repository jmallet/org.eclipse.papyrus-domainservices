/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.scope;

import static java.util.stream.Collectors.joining;
import static org.eclipse.papyrus.uml.domain.services.EMFUtils.allContainedObjectOfType;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Set;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.internal.resource.UMLResourceImpl;
import org.eclipse.uml2.uml.resource.UMLResource;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementRootCandidateSeachProvider}.
 * 
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
@SuppressWarnings("restriction")
class ElementRootCandidateSeachProviderTest extends AbstractUMLTest {

    /**
     * Model containing a UML Model with stereotype content.
     */
    private static final URI UML_MODEL_WITH_PROFILE = URI.createPlatformPluginURI(
            "/org.eclipse.papyrus.uml.domain.services.test/profile/UMLModelWithStandardProfile.uml", false);

    private static final String PACK3_1 = "Pack3.1";
    private static final String PACK3 = "Pack3";
    private static final String PROFILE2 = "Profile2";
    private static final String PACK2_1 = "Pack2.1";
    private static final String PACK2 = "Pack2";
    private static final String PACK1 = "Pack1";
    private static final String MODEL2 = "Model2";
    private static final String FAKE_URI2 = "fake://test2";
    private static final String MODEL1 = "Model1";
    private static final String CLASS1 = "Class1";
    private static final String FAKE_URI1 = "fake://test";

    /**
     * Check there is no failure on null.
     */
    @Test
    public void nullElement() {
        assertFrom(null).hasReachableRoots();
    }

    /**
     * Check the behavior on standalone element.
     */
    @Test
    public void soloElement() {
        var clazz = create(Class.class);
        assertFrom(clazz).hasReachableRoots(clazz);
    }

    /**
     * Check the behavior on element in a {@link Resource}.
     */
    @Test
    public void soloElementInResource() {

        UMLResource resource = new UMLResourceImpl(URI.createURI(FAKE_URI1));
        var clazz = createNamedElement(Class.class, CLASS1);
        resource.getContents().add(clazz);

        assertFrom(clazz).hasReachableRoots(clazz);
    }

    /**
     * Check when the dependency is defined at root level.
     */
    @Test
    public void rootModelDependency() {
        UMLResource resource1 = new UMLResourceImpl(URI.createURI(FAKE_URI1));
        var rootModel1 = createNamedElement(Model.class, MODEL1);
        resource1.getContents().add(rootModel1);

        var clazz = createNamedElement(Class.class, CLASS1);
        rootModel1.getPackagedElements().add(clazz);

        UMLResource resource2 = new UMLResourceImpl(URI.createURI(FAKE_URI2));
        var rootModel2 = createNamedElement(Model.class, MODEL2);
        resource2.getContents().add(rootModel1);

        createPackageImport(rootModel1, rootModel2);

        assertFrom(clazz).hasReachableRoots(rootModel1, rootModel2);

        assertFrom(rootModel1).hasReachableRoots(rootModel1, rootModel2);

        assertFrom(rootModel2).hasReachableRoots(rootModel2);

    }

    /**
     * Check when the dependency define a loop on self.
     */
    @Test
    public void rootModelDependencySelfLoop() {
        UMLResource resource1 = new UMLResourceImpl(URI.createURI(FAKE_URI1));
        var rootModel1 = createNamedElement(Model.class, MODEL1);
        resource1.getContents().add(rootModel1);

        var clazz = createNamedElement(Class.class, CLASS1);
        rootModel1.getPackagedElements().add(clazz);

        createPackageImport(rootModel1, rootModel1);

        assertFrom(clazz).hasReachableRoots(rootModel1);
        assertFrom(rootModel1).hasReachableRoots(rootModel1);
    }

    /**
     * Check when the dependency define a loop at root level with more than one
     * model.
     */
    @Test
    public void rootModelDependencyLoop() {
        UMLResource resource1 = new UMLResourceImpl(URI.createURI(FAKE_URI1));
        var rootModel1 = createNamedElement(Model.class, MODEL1);
        resource1.getContents().add(rootModel1);

        var clazz = createNamedElement(Class.class, CLASS1);
        rootModel1.getPackagedElements().add(clazz);

        UMLResource resource2 = new UMLResourceImpl(URI.createURI(FAKE_URI2));
        var rootModel2 = createNamedElement(Model.class, MODEL2);
        resource2.getContents().add(rootModel1);

        createPackageImport(rootModel1, rootModel2);
        createPackageImport(rootModel2, rootModel1);

        assertFrom(clazz).hasReachableRoots(rootModel1, rootModel2);

        assertFrom(rootModel1).hasReachableRoots(rootModel1, rootModel2);

        assertFrom(rootModel2).hasReachableRoots(rootModel1, rootModel2);

    }

    /**
     * Check when the dependency define a loop in a nested level with more than one
     * model.
     */
    @Test
    public void nestedLoop() {
        UMLResource resource1 = new UMLResourceImpl(URI.createURI(FAKE_URI1));
        var rootModel1 = createNamedElement(Model.class, MODEL1);
        resource1.getContents().add(rootModel1);

        var pack1 = createNamedElement(Package.class, PACK1);
        rootModel1.getPackagedElements().add(pack1);

        UMLResource resource2 = new UMLResourceImpl(URI.createURI(FAKE_URI2));
        var rootModel2 = createNamedElement(Model.class, MODEL2);
        resource2.getContents().add(rootModel2);

        var pack2 = createNamedElement(Package.class, PACK2);
        rootModel2.getPackagedElements().add(pack2);

        createPackageImport(pack1, rootModel2);
        createPackageImport(pack2, rootModel1);

        assertFrom(rootModel1).hasReachableRoots(rootModel1);
        assertFrom(pack1).hasReachableRoots(rootModel1, rootModel2);
        assertFrom(pack2).hasReachableRoots(rootModel1, rootModel2);
        assertFrom(rootModel2).hasReachableRoots(rootModel2);

    }

    /**
     * Check the behavior when nesting 3 level of import.
     */
    @Test
    public void nestedImport3Level() {
        UMLResource resource1 = new UMLResourceImpl(URI.createURI(FAKE_URI1));
        var rootModel1 = createNamedElement(Model.class, MODEL1);
        resource1.getContents().add(rootModel1);

        var pack1 = createNamedElement(Package.class, PACK1);
        rootModel1.getPackagedElements().add(pack1);
        var class1 = createNamedElement(Class.class, CLASS1);
        pack1.getPackagedElements().add(class1);

        UMLResource resource2 = new UMLResourceImpl(URI.createURI(FAKE_URI2));
        var rootModel2 = createNamedElement(Model.class, MODEL2);
        resource2.getContents().add(rootModel2);

        var pack2 = createNamedElement(Package.class, PACK2);
        rootModel2.getPackagedElements().add(pack2);

        var pack21 = createNamedElement(Package.class, PACK2_1);
        pack2.getPackagedElements().add(pack21);

        createPackageImport(pack1, pack21);

        UMLResource resource3 = new UMLResourceImpl(URI.createURI("fake://test3"));
        var rootModel3 = createNamedElement(Profile.class, PROFILE2);
        resource3.getContents().add(rootModel3);

        var pack3 = createNamedElement(Package.class, PACK3);
        rootModel3.getPackagedElements().add(pack3);

        var pack31 = createNamedElement(Package.class, PACK3_1);
        pack3.getPackagedElements().add(pack31);

        createPackageImport(pack21, pack31);
        createPackageImport(pack31, pack1);

        assertFrom(class1).hasReachableRoots(rootModel1, pack21, pack31);

    }

    @Test
    public void nestedInside() {
        UMLResource resource1 = new UMLResourceImpl(URI.createURI(FAKE_URI1));
        var rootModel1 = createNamedElement(Model.class, MODEL1);
        resource1.getContents().add(rootModel1);
        Package parent = rootModel1;
        for (int i = 1; i < 5; i++) {
            parent = createNestedImportedPackage(parent, i);
            assertFrom(parent).hasReachableRoots(rootModel1);
        }

    }

    /**
     * Checks the accessible roots from a Stereotype Application
     */
    @Test
    public void stereotypeApplication() {
        ResourceSet rs = new ResourceSetImpl();
        Resource umlResource = rs.getResource(UML_MODEL_WITH_PROFILE, true);

        EObject model = umlResource.getContents().get(0);
        EObject primitiveTypeModel = rs
                .getEObject(URI.createURI("pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml#_0"), true);

        String classTwoStereotypeName = "ClassTwoStereotypes";
        Class classTwoStereotype = allContainedObjectOfType(umlResource, Class.class)
                .filter(c -> classTwoStereotypeName.equals(c.getName())).findFirst().orElseThrow();

        EList<EObject> steretopesApplications = classTwoStereotype.getStereotypeApplications();

        for (EObject stereotypeApplication : steretopesApplications) {
            assertFrom(stereotypeApplication).hasReachableRoots(model, primitiveTypeModel);
        }
    }

    private Package createNestedImportedPackage(Package parent, int i) {
        var pack1 = createNamedElement(Package.class, "Pack" + i);
        parent.getPackagedElements().add(pack1);

        createPackageImport(parent, pack1);
        createPackageImport(pack1, parent);

        return pack1;
    }

    private void createPackageImport(Namespace source, Package target) {
        PackageImport pImport = UMLFactory.eINSTANCE.createPackageImport();
        pImport.setImportedPackage(target);
        source.getPackageImports().add(pImport);
    }

    private ReachableTest assertFrom(EObject source) {
        return new ReachableTest(source);
    }

    private static final class ReachableTest {

        private final EObject source;

        private final ElementRootCandidateSeachProvider provider = new ElementRootCandidateSeachProvider();

        private ReachableTest(EObject source) {
            super();
            this.source = source;
        }

        public void hasReachableRoots(EObject... expectecRoot) {
            var reachableElements = provider.getReachableRoots(source);
            Set<EObject> expected = Set.of(expectecRoot);
            Set<Object> result = Set.copyOf(reachableElements);
            assertEquals(expected, result,
                    "expected {" + expected.stream().map(e -> ((NamedElement) e).getName()).collect(joining(","))
                            + "} != result {"
                            + result.stream().map(e -> ((NamedElement) e).getName()).collect(joining(",")) + "}");
        }
    }

}
