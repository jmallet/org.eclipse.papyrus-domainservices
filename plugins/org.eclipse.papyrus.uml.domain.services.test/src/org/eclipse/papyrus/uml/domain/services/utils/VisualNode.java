/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

public class VisualNode {

    private final EObject semanticElement;

    private final List<VisualNode> children = new ArrayList<>();
    private final List<VisualNode> borderNodes = new ArrayList<>();

    private VisualNode parent;

    /**
     * Constructor.
     * 
     * @param semanticElement
     *                        the semantic element to represent
     */
    public VisualNode(EObject semanticElement) {
        super();
        this.semanticElement = semanticElement;
    }

    public EObject getSemanticElement() {
        return semanticElement;
    }

    public VisualNode addChildren(EObject theSemanticElement) {
        VisualNode node = new VisualNode(theSemanticElement);
        node.setParent(this);
        children.add(node);
        return node;
    }

    public VisualNode addBorderNode(EObject theSemanticElement) {
        VisualNode node = new VisualNode(theSemanticElement);
        node.setParent(this);
        borderNodes.add(node);
        return node;
    }

    private void setParent(VisualNode parent) {
        this.parent = parent;
    }

    public VisualNode getParent() {
        return parent;
    }

    public List<VisualNode> getChildren() {
        return children;
    }

    public List<VisualNode> getBorderNodes() {
        return borderNodes;
    }
}
