/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo
 *
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *    Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.modify;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.Status;

/**
 * Object in charge of modifying a feature on an {@link EObject}s.
 * 
 * @author Arthur Daussy
 *
 */
public interface IFeatureModifier {

    /**
     * Set a <b>unique</b> value of a feature. A set operation always override the
     * current value. If the feature is many then the collection is cleared and then
     * the new value is added (so it is the only value left in the collection at the
     * end of the operation). If the feature is unary then the feature is simply
     * set.
     * 
     * @param featureOwner
     *                     the object to modify
     * @param featureName
     *                     the name of the <b>unary</b> feature
     * @param newValue
     *                     the new value to set
     * @return a {@link Status}
     */
    Status setValue(EObject featureOwner, String featureName, Object newValue);

    /**
     * Adds a <b>unique</b> value to a feature. If the feature is <b>many</b> then
     * the value is added to the collection otherwise the feature is set with the
     * given value.
     * 
     * 
     * @param featureOwner
     *                     the object to modify
     * @param featureName
     *                     the name of the <b>unary</b> feature
     * @param newValue
     *                     the new value to add
     * @return a Status
     */
    Status addValue(EObject featureOwner, String featureName, Object newValue);

    /**
     * Removes a <b>unique</b> value from a feature containing possible multiple
     * values.
     * 
     * <p>
     * <b>Be aware</b> that removing a value from a unary feature always return a
     * failing status. In order to remove a value from an unary feature use
     * {@link #setValue(EObject, String, Object)} with the default value.
     * </p>
     * 
     * @param featureOwner
     *                     the object to modify
     * @param featureName
     *                     the name of the <b>unary</b> feature
     * @param oldValue
     *                     the new value to remove
     * @return a {@link Status}
     */
    Status removeValue(EObject featureOwner, String featureName, Object oldValue);

}
