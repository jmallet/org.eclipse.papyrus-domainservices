/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.IViewQuerier;

/**
 * Provides a semantic container for the semantic element representing a domain
 * based edge using its semantic source and target.
 *
 * @author Arthur Daussy
 */
public interface IDomainBasedEdgeContainerProvider {

    /**
     * Gets the container of a semantic element represented as an Edge.
     *
     * @param semanticSource
     *                       the source of the edge
     * @param semanticTarget
     *                       the target of the edge
     * @param semanticEdge
     *                       the semantic element of the edge
     * @return a container or <code>null</code> is enable to compute a container
     */
    EObject getContainer(EObject semanticSource, EObject semanticTarget, EObject semanticEdge, IViewQuerier querier,
            Object sourceNode, Object targetNode);

    class NoOP implements IDomainBasedEdgeContainerProvider {

        @Override
        public EObject getContainer(EObject semanticSource, EObject semanticTarget, EObject semanticEdge,
                IViewQuerier querier, Object sourceNode, Object targetNode) {
            return null;
        }

    }

}
