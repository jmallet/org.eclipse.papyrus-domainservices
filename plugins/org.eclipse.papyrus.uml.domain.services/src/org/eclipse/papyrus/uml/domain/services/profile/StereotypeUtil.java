/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.profile;

import java.util.Optional;
import java.util.stream.Stream;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.UMLPackage;

/**
 * A class containing static utility methods regarding UML stereotypes.<br/>
 * 
 * @author Arthur Daussy
 */
public class StereotypeUtil {

    /**
     * Check if the given {@link EObject} is a stereotype application.
     * 
     * @param self
     *             an EObject
     * @return <code>true</code> if is a StereotypeApplication, <code>false</code>
     *         otherwise
     */
    public static boolean isStereotypeApplication(EObject self) {
        return self != null && getInUsedBaseReference(self).isPresent();
    }

    /**
     * Gets the "base_" EReference used to link the given stereotype to its base
     * element.
     * 
     * @param self
     *             a possible stereotype application
     * @return an optional {@link EReference}
     */
    public static Optional<EReference> getInUsedBaseReference(EObject self) {
        return getBaseReferences(self.eClass()).filter(ref -> self.eGet(ref) instanceof Element).findFirst();
    }

    /**
     * Gets a stream of all EReference in the given EClass that is used to reference
     * the base element from a stereotype application.
     * 
     * @param eClass
     *               the EClass of a stereotype application
     * @return a stream
     */
    public static Stream<EReference> getBaseReferences(EClass eClass) {
        return eClass.getEAllReferences().stream().filter(ref -> ref.getName().startsWith("base_") && !ref.isMany()
                && ref.getEType().getEPackage() == UMLPackage.eINSTANCE);
    }

    /**
     * Gets the base element of a Stereotype application.
     * 
     * @param steretoypeApplication
     *                              a Stereotype application
     * @return a Element (or <code>null</code>)
     */
    public static Element getBaseElement(EObject steretoypeApplication) {
        Optional<EReference> inUsedBaseReference = StereotypeUtil.getInUsedBaseReference(steretoypeApplication);
        return inUsedBaseReference.map(ref -> (Element) steretoypeApplication.eGet(ref)).orElse(null);
    }

    /**
     * Gets the Stereotype application of a base element given its type.
     * 
     * @param element
     *                                  the base element
     * @param stereotypeApplicationType
     *                                  the type of the expected stereotype
     * @return a Stereotype application (or <code>null</code>)
     */
    public static EObject getStereotypeApplication(Element element, EClass stereotypeApplicationType) {
        return element.getStereotypeApplications().stream()
                .filter(st -> stereotypeApplicationType.isSuperTypeOf(st.eClass())).findFirst().orElse(null);
    }

}
