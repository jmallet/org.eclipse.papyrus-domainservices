/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.directedit;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;

/**
 * Service in charge of consuming a new text after the execution of a direct
 * edit tool.
 * 
 * @author Arthur Daussy
 *
 */
public interface IDirectEditValueConsumer {

    /**
     * Consume the label entered by the user after the execution of direct edit
     * tool.
     * 
     * @param owner
     *                   the semantic object on which the direct tool has been
     *                   executed
     * @param editedText
     *                   the text entered by the user
     * @return a valid {@link CheckStatus}, a failing status if the given value is
     *         invalid.
     */
    CheckStatus consumeNewLabel(EObject owner, String editedText);

}
