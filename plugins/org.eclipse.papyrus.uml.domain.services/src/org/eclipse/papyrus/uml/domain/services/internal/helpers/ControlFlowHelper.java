/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.internal.helpers;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.uml2.uml.ActivityNode;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.FinalNode;
import org.eclipse.uml2.uml.InitialNode;
import org.eclipse.uml2.uml.ObjectNode;

/**
 * Helper dedicated to the {@link ControlFlow} concept.
 * 
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 */
public class ControlFlowHelper {
    /**
     * Checks if we can Create an {@link ControlFlow} between the given source and
     * target.
     * 
     * @param semanticEdgeSource
     *                           the candidate edge source.
     * @param semanticEdgeTarget
     *                           the candidate edge target.
     * @return true if we can create the {@link ControlFlow}, false otherwise.
     */
    public boolean canCreateControlFlow(EObject semanticEdgeSource, EObject semanticEdgeTarget) {
        return canCreateFromSource(semanticEdgeSource) && canCreateToTarget(semanticEdgeTarget);
    }

    /**
     * Checks if we can Create an {@link ControlFlow} from the given source.
     * 
     * @param semanticEdgeSource
     *                           the candidate edge source.
     * @return true if we can create the {@link ControlFlow}, false otherwise.
     */
    public boolean canCreateFromSource(EObject semanticEdgeSource) {
        return (semanticEdgeSource instanceof ActivityNode) && !(semanticEdgeSource instanceof FinalNode)
                && isValidObjectNode(semanticEdgeSource);
    }

    private boolean canCreateToTarget(EObject semanticEdgeTarget) {
        return (semanticEdgeTarget instanceof ActivityNode) && !(semanticEdgeTarget instanceof InitialNode)
                && isValidObjectNode(semanticEdgeTarget);
    }

    private boolean isValidObjectNode(EObject semanticEdgeSource) {
        // A ObjectNode is a valid source or target if it is a Control Type.
        return !(semanticEdgeSource instanceof ObjectNode) || ((ObjectNode) semanticEdgeSource).isControlType();
    }
}
