/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import org.eclipse.emf.ecore.EObject;

/**
 * Object in charge of creating a semantic element.
 *
 * @author Arthur Daussy
 */
public interface ICreator {

    /**
     * Creates a new semantic parent.
     *
     * @param parent
     *                                  container of the new elements (might be
     *                                  changed in the implementation)
     * @param type
     *                                  type of the new element (ex "Class" or
     *                                  "uml::Class")
     * @param containementReferenceName
     *                                  name of the containment reference to be used
     *                                  to attach the new element to the model
     * @return a {@link CreationStatus}S
     */
    CreationStatus create(EObject parent, String type, String containementReferenceName);

}
